# Applying For First Time US Passport

If you are applying for your first U.S. Passport, you must apply in person at one of more than 5,000 facilities, such as Clerks of Court and Post Offices, which accept passport applications. To find your nearest acceptance facility, visit travel.state.gov or contact the National Passport Information Center at 877/487-2778. You will need to provide the following documents:



1. One item of proof of United States citizenship or nationality such as: an expired U.S. passport, a certified copy of a birth certificate (one issued from a government office, not a hospital; click here to find out how to obtain one) for all applicants born in the U.S., a Certificate of Naturalization, Certificate of Citizenship, or a Consular Report of Birth Abroad. Bring a one-sided photocopy as well.

2. Proof of identity (photo ID with signature) such as a previous or current U.S. passport, a Certificate of Naturalization or Citizenship, a valid driver's license, or a valid government or military identification card. Here's a list of other acceptable IDs. Bring a one-sided photocopy as well.



Read More:- https://nationalpassportservice.com/



3. One clear passport photograph taken within the last six months. Do not re-use ID photos that were taken before then. The photographs must be 2x2 inches (51 mm x 51 mm) with and your head should be about 1 and 1 3/8 inches (25–25 mm) tall (digital images must be between 600x600 pixels and 1200x1200 pixels; there's a cropping tool on the State Department's website). Photographs must be in color of a front view, full face, taken in normal street attire (no uniforms or camouflage) without a hat or glasses, with a plain white or off-white background. Someone else must take the photo; no selfies are permitted. No photo filters may be used, and no photos with red eye may be used. You may have a natural smile and you may wear jewelry or piercings that do no obstruct your face. See the State Department's Photo Tool for examples.

4. A completed passport application form DS-11 which contains all the requested information except your signature. This form must be signed in the presence of an authorized executing official.

After you apply, you can track the status of your application online at passportstatus.state.gov. If your status reads "Not Available," don't panic, because it may take up to four weeks for your status to be changed to "In Process."

Once your status is "Mailed," it should take about 10 days to receive your new passport. 



In 2021, the process took 10–12 weeks for standard ("routine") service and four to six weeks for for "expedited" service that cost an extra $60.
